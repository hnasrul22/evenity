/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}"],
  theme: {
    extend: {},
    colors : {
      white : '#ffffff',
      purple : '#070148',
      gray : '#6B7280',
      black : '#000000',
      'dark-gray' : '#171717',
      transparent : '#00000000'
    },
    fontFamily : {
      'fredoka' : ['Fredoka One', 'cursive'],
      'inter' : ['Inter', 'sans-serif'],
      'rubik' : ['Rubik', 'sans-serif']
    }
  },
  plugins: [],
}
