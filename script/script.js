const toogleNav = document.getElementById('toogle-nav')
const menu = document.getElementById("menu");
const days = document.getElementById("days");
const hours = document.getElementById("hours");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds")

AOS.init()

toogleNav.onclick = () => {
    menu.classList.toggle("top-0")
    menu.classList.toggle("-top-full")    
}

const countDownDate = new Date("May 4, 2023 15:37:25").getTime();

// Update the count down every 1 second
const x = setInterval(() => {

  // Get today's date and time
  const now = new Date().getTime();

  // Find the distance between now and the count down date
  const distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  const d = Math.floor(distance / (1000 * 60 * 60 * 24));
  const h = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const m = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const s = Math.floor((distance % (1000 * 60)) / 1000);

  // Display
  days.innerHTML = d;
  hours.innerHTML = h;
  minutes.innerHTML = m;
  seconds.innerHTML = s;
  

}, 1000);